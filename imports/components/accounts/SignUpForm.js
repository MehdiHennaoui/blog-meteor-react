import React, { PureComponent } from "react";
import { Meteor } from "meteor/meteor";
import { Form, Input, Button } from "semantic-ui-react";

export default class extends PureComponent {
    state = {
        username: "",
        email: "",
        password: "",
        passwordConf: ""
    };
    initialState = {};
    componentDidMount = () => (this.initialState = { ...this.state });
    handleChange = evt => this.setState({ [evt.target.name]: evt.target.value });
    handleSubmit = evt => {
        evt.preventDefault();
        Meteor.call("accounts.signup", this.state, (err, res) => {
            if (err) console.error(err);
            else
                Meteor.loginWithPassword(
                    this.state.email,
                    this.state.password,
                    (err, res) => this.setState(this.initialState)
                );
        });
    };
    render() {
        const { username, email, password, passwordConf } = this.state;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Field>
                    <label>Username</label>
                    <Input
                        type="text"
                        name="username"
                        value={username}
                        onChange={this.handleChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Email</label>
                    <Input
                        type="email"
                        name="email"
                        value={email}
                        onChange={this.handleChange}
                    />
                </Form.Field>

                <Form.Field>
                    <label>Password</label>
                    <Input
                        type="password"
                        name="password"
                        value={password}
                        onChange={this.handleChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Password confirmation</label>
                    <Input
                        type="password"
                        name="passwordConf"
                        value={passwordConf}
                        onChange={this.handleChange}
                    />
                </Form.Field>
                <Button
                    type="submit"
                    disabled={
                        !Boolean(
                            username.length &&
                            email.length &&
                            password.length &&
                            passwordConf.length &&
                            password === passwordConf
                        )
                    }
                >
                    Signup
                </Button>
            </Form>
        );
    }
}