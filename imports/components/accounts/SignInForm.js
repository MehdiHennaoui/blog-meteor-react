import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { Meteor } from "meteor/meteor";
import { Form, Input, Button } from "semantic-ui-react";

class SignInForm extends PureComponent {
    state = {
        username: "",
        email: "",
        password: "",
        passwordConf: ""
    };
    initialState = {};
    componentDidMount = () => (this.initialState = { ...this.state });
    handleChange = evt => this.setState({ [evt.target.name]: evt.target.value });
    handleSubmit = evt => {
        evt.preventDefault();
        Meteor.loginWithPassword(
            this.state.email,
            this.state.password,
            (err, res) => {
                if (err) console.error(err.stack);
                else {
                    this.setState(this.initialState);
                    this.props.history.push("/");
                }
            }
        );
    };
    render() {
        const { email, password } = this.state;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Field>
                    <label>Email</label>
                    <Input
                        type="email"
                        name="email"
                        value={email}
                        onChange={this.handleChange}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Password</label>
                    <Input
                        type="password"
                        name="password"
                        value={password}
                        onChange={this.handleChange}
                    />
                </Form.Field>

                <Button
                    type="submit"
                    disabled={!Boolean(email.length && password.length)}
                >
                    Sign In
                </Button>
            </Form>
        );
    }
}

export default withRouter(SignInForm);