import React, {Component} from 'react';
import {Menu, Icon, Button} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import {withTracker} from 'meteor/react-meteor-data';

class Navbar extends Component {

    onDisconnect = () => {
        Meteor.logout();
    };

    render() {
        const {admin, user} = this.props;
        const custom_classes = admin && "white-item";
        return (
            <Menu borderless style={{backgroundColor: admin && "grey"}}>
                { user && Roles.userIsInRole(user._id, 'admin') &&
                [
                    <Menu.Item className={custom_classes} style={{color: admin && "white"}}>
                        ADMIN
                    </Menu.Item>,
                    <Link to="/admin/pages">
                        <Menu.Item className={custom_classes}>
                            Admin pages
                        </Menu.Item>
                    </Link>
                ]
                }
                <Link to="/">
                    <Menu.Item className={custom_classes} style={{color: admin && "white"}}>
                        Accueil
                    </Menu.Item>
                </Link>
                <Menu.Menu position="right">
                    {!user &&
                    <Menu.Item style={{color: admin && "white"}}>
                        <Link to="/connexion">
                            Connexion
                        </Link>
                    </Menu.Item>
                    }
                    {user &&
                    [
                        <Menu.Item>
                            <Icon name="user circle outline">
                                {user.username}
                            </Icon>
                        </Menu.Item>,
                        <Menu.Item position="right" className={custom_classes}>
                            <Button onClick={this.onDisconnect}>
                                Déconnexion
                            </Button>
                        </Menu.Item>
                    ]
                    }
                </Menu.Menu>
            </Menu>

        )
    }
}

export default NavbarContainer = withTracker(() => {
    const user = Meteor.user();
    return {
        user
    }
})(Navbar)
