import React, { Component } from 'react';
import { Form, Input, Button } from 'semantic-ui-react';



export default class DynamicPageForm extends Component {

    state = {
        page: {}
    }

    handleChange = (event) => {
        this.setState({
             page: {
                 ...this.state.page,
                 [event.target.name]: event.target.value
             }
        })
    }

    submit_form = (e) => {
        e.preventDefault();

        const method = this.props.page ?"update" : "insert"

            Meteor.call('dynamic_pages.' + method, this.state.page, (error, result) => {
                if(error) {
                    Bert.alert({
                        title: 'Erreur',
                        message: error.message,
                        type: 'error',
                        style: 'growl-bottom-left'
                    })
                }else {
                    Bert.alert({
                        title: this.props.page ? "page modifié" : "page créée",
                        message: result,
                        type:'sucess',
                        style: 'growl-bottom-left'
                    })
                    this.setState({});
                    this.props.onSubmitForm && this.props.onSubmitForm()
                }
            })
    };

    componentDidMount() {
        if(this.props.page) {
            this.setState({page: this.props.page})
        }
    };

    componentWillReceiveProps(new_props) {
        if(this.props.page) {
            this.setState({page: new_props.page})
        }
    };

    render() {
        const {page} = this.state;
        const { title, description, image_url } = this.props;
        return (
            <Form onSubmit={this.submit_form}>
                <Form.Field>
                    <label>Titre de la page</label>
                    <Input ref="title" type="text" onChange={this.handleChange} name="title" value={page.title} required/>
                </Form.Field>
                <Form.Field>
                    <label>description</label>
                    <Input ref="title" type="description" onChange={this.handleChange} name="description" value={page.description} />
                </Form.Field>
                <Form.Field>
                    <label>image_url</label>
                    <Input ref="title" type="image_url" onChange={this.handleChange} name="image_url" value={page.image_url} label="http://"/>
                </Form.Field>
                <Form.Field>
                    <Button color="green">
                        {this.props.page ? "éditer": "Créer"}
                    </Button>
                </Form.Field>
            </Form>
        )
    }
}