import React, {Component} from 'react'
import { Card, Image, Button } from 'semantic-ui-react'
import {Link} from 'react-router-dom'
class CardExport extends Component {

    render() {
        const { page: {_id,image_url, description, title} } = this.props;
        return(
            <Card  {...this.props}>
                {image_url ? <Image  src={image_url} size="small" /> : <Image  src={"https://i.pinimg.com/736x/c5/09/6f/c5096f79771641c133dd46163d933f59--blue-eyes-crazy-cats.jpg"} size="small" />}
                <Card.Header>
                    <p>{title}</p>
                </Card.Header>
                <Card.Description>
                    <p>{description}</p>
                </Card.Description>
                <Button color="green"><Link to={"/article/"+ _id}>Voir la page</Link></Button>
            </Card>
        )}
}


export default CardExport;