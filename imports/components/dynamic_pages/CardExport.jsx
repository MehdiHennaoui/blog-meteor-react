import React, {Component} from 'react'
import { Card, Image, Button } from 'semantic-ui-react'

class CardExport extends Component {

    deletePage = () => {
        Meteor.call('dynamic_pages.remove', this.props.page._id, (error, result) => {
            Bert.alert()
        });
        this.props.onSubmitForm && this.props.onSubmitForm;
    };

    edit_page = () => this.props.onEditClick(this.props.page);

    render() {
        const { page: {image_url, description, title} } = this.props;
        return(
            <Card  {...this.props}>
                {image_url ? <Image  src={image_url} size="small" /> : <Image  src={"https://i.pinimg.com/736x/c5/09/6f/c5096f79771641c133dd46163d933f59--blue-eyes-crazy-cats.jpg"} size="small" />}
                <Card.Header>
                    <p>{title}</p>
                </Card.Header>
                <Card.Description>
                    <p>{description}</p>
                </Card.Description>
                <Button color="blue" onClick={this.edit_page} content="Editer"/>
                <Button color="red" onClick={this.deletePage} content="Suprimer" />
            </Card>
        )}
    }


export default CardExport;