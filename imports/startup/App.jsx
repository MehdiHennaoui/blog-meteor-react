import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import AdminLayout from '../layout/AdminLayout';
import MainLayout from '../layout/MainLayout';
import 'semantic-ui-css/semantic.min.css';
import ConnexionForm from '/imports/api/accounts/ConnexionForm';

export default class App extends Component {
    render() {
        return(
            <Router>
                <Switch>
                    <Route path="/admin" component={AdminLayout} />
                    <Route path="*" component={MainLayout} />
                </Switch>
            </Router>
        )
    }
}