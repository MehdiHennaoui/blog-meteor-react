import React, {Component} from 'react';
import {withTracker} from 'meteor/react-meteor-data';
import {DynamicPages} from '/imports/api/dynamic_pages/dynamic_pages'
import {Grid, Form, Button, Loader, Input, Container, Card} from 'semantic-ui-react';
import SignUpForm from '/imports/api/accounts/SignUpForm';
import CardLandingPage from "../../components/dynamic_pages/CardLandingPages";

export class Landing extends Component {

    state = {
        title: '',
        description: ''
    };

    create_page = (event) => {
        event.preventDefault();

        Meteor.call('dynamic_pages.insert', this.state, (error, result) => {
            if (error) {
                alert('ERREUR de creation de page' + error);
            } else {
                console.log('Nouvelle page ajoutée');
                this.setState({
                    title: '',
                    description: ''
                })
            }
        });
    };

    handleChange = (attr, e) => {
        const state = this.state;
        state[attr] = e.target.value;
        this.setState(state)
    };



    render() {
        const {title, description} = this.state;
        const {loading, dynamic_pages, user_id} = this.props;

        if (loading) {
            return <Loader/>
        } else {
            return (
            <Container>
                <Grid stackable>
                    <Grid.Column widescreen={8} color="blue">
                        <h1>Landing page</h1>
                        {
                            dynamic_pages.map((page) => {
                                return <CardLandingPage key={page._id} page={page}/>
                            })
                        }
                    </Grid.Column>
                    <Grid.Column widescreen={16}>
                        {!user_id &&
                            <SignUpForm />
                        }
                    </Grid.Column>
                </Grid>
            </Container>
            )
        }

    }
}

export default LandingContainer = withTracker(() => {
    const dynamicPagesPublication = Meteor.subscribe('dynamic_pages.all');
    const loading = !dynamicPagesPublication.ready();
    const dynamic_pages = DynamicPages.find({}).fetch();
    const user_id = Meteor.userId();
    return {
        loading,
        dynamic_pages,
        user_id,
    }
})(Landing)