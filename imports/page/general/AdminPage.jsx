import React, {Component} from 'react';
import {Container, Grid, Header, Loader, Image, Card, Button} from 'semantic-ui-react';
import {DynamicPages} from '/imports/api/dynamic_pages/dynamic_pages';
import {withTracker} from 'meteor/react-meteor-data';
import DynamicPagesForm from '../../components/dynamic_pages/DynamicPageForm';
import CardExport from '../../components/dynamic_pages/CardExport';


class AdminPage extends Component {
    state = {
        display_form: false,
    };

    toggleForm = (e) => {
        this.setState({
            display_form: !this.state.display_form,
            editing_page: null,
        })
    };

    onFormSubmit = (e) => {
        this.setState({display_form: false})
    };

    editPage = (editing_page) => {
        this.setState({editing_page, display_form: true})
    };

    render() {

        const {loading, dynamic_pages} = this.props;
        const {display_form, editing_page} = this.state;

        if (!loading) {
            return (
                <Container>
                    <Header as="h1" textAlign='center'>Admin page</Header>
                    <Grid stackable>
                        <Button onClick={this.toggleForm} name="display_form">
                            {display_form ? "Annuler" : "créer une page"}
                        </Button>

                        {display_form && <DynamicPagesForm page={editing_page} onSubmitForm={this.onFormSubmit}/>}
                        <Card.Group>
                            {
                                dynamic_pages.map((page) => {
                                    return (
                                        <CardExport key={page._id} page={page} onEditClick={this.editPage}/>
                                    )
                                })
                            }
                        </Card.Group>
                    </Grid>
                </Container>
            )
        } else {
            return (
                <Loader/>
            )
        }
    }
}

export default AdminPageContainer = withTracker(() => {
    const dynamicPagesPublication = Meteor.subscribe('dynamic_pages.all');
    const loading = !dynamicPagesPublication.ready();
    const dynamic_pages = DynamicPages.find({}).fetch();
    return {
        loading,
        dynamic_pages
    }
})(AdminPage);

