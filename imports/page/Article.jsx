import React, {Component} from 'react'
import { Card, Image, Button, Container, Header, Grid } from 'semantic-ui-react'
import {DynamicPages} from '/imports/api/dynamic_pages/dynamic_pages'
import { withTracker } from 'meteor/react-meteor-data';



class Article extends Component {
    render() {

        const {  loading, page } = this.props;

            if(!loading) {

                return(
                  <Container>
                      <Grid stackable>
                      <Image  src={page.image_url ? page.image_url : 'https://cdn.browshot.com/static/images/not-found.png'} size="big" />
                    <Header textAlign="center">
                        {page.title}
                    </Header>
                    <p>{page.description}</p>
                      </Grid>
                  </Container>
                )
            }else {
                return <div>Chargement</div>
            }

        }
}


export default Article = withTracker(({match}) => {
    const { id } = match.params;
    const pagePublication = Meteor.subscribe('dynamic_pages.by_id', id);
    const loading = !pagePublication.ready();
    const page = DynamicPages.findOne({_id: id});
    console.log('container', page, id)
    return {
        loading,
        page,
    }
})(Article)