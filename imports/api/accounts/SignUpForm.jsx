import React, {Component} from 'react';
import {Form, Input, Button} from 'semantic-ui-react';

export default class SignUpForm extends Component {

    state = {
        username: "",
        email: "",
        password: "",
        password_confirmation: ""
    }

    handleChange = e => {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    };

    submitForm = (event) => {
        event.preventDefault();
        Meteor.call('account.signup', this.state, (error, result) => {
            if(error) {
                alert("ERREUR LORS DE L'INSCRIPTION: " + error);
            } else {
                Meteor.loginWithPassword(this.state.email, this.state.password, (error, result) => {
                    if(error) {
                        console.log('Erreur de la connection')
                    } else {
                        this.setState({})
                    }

                })
            }

        })
    }

    render() {
        const {username, email, password, password_confirmation} = this.state;
        const form_valid = password && (password == password_confirmation) && username.length > 5;
        return (
            <Form onSubmit={this.submitForm}>
                <Form.Field>
                    <label htmlFor="">Username</label>
                    <Input type="text" value={username} name="username" onChange={this.handleChange}/>
                    {username.length < 5 && <label>Username doit avoir au moins 5 caractères</label> }
                </Form.Field>
                <Form.Field>
                    <label htmlFor="">email</label>
                    <Input type="email" value={email} name="email" onChange={this.handleChange}/>
                </Form.Field>
                <Form.Field>
                    <label htmlFor="">Password</label>
                    <Input type="password" value={password} name="password" onChange={this.handleChange}/>
                </Form.Field>
                <Form.Field>
                    <label htmlFor="">Password confirmation</label>
                    <Input type="password" value={password_confirmation} name="password_confirmation" onChange={this.handleChange}/>
                    { password ==! password_confirmation ? <label>Le password et la confirmation password ne sont pas pareil</label> : ""}
                </Form.Field>
                <Form.Field>
                    <Button color="green" disabled={!form_valid}>S'incrire</Button>
                </Form.Field>
            </Form>
        )
    }
}