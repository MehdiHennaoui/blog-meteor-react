import React, { Component } from 'react';
import { Form, Input, Button, Container} from 'semantic-ui-react';

export default class ConnexionForm extends Component {
    state= {
       email: '',
       password: ''
    };

    handleChange = e => {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    };

    submitForm = () => {
        Meteor.loginWithPassword(this.state.email, this.state.password, (error, result) => {
            if(error) {
                console.log("error: ", error)
            } else {
                console.log("good");
            }
        })
    }

    render() {
        const { email, password } = this.state;
       return(
       <Container>
           <Form onSubmit={this.submitForm}>
            <Form.Field>
                <Input type="mail" label="nom d'utilisateur" name="email" value={email} onChange={this.handleChange}/>
            </Form.Field>
            <Form.Field>
                <Input type="password" label="mot de passe" name="password" value={password} onChange={this.handleChange}/>
            </Form.Field>
               <Form.Field>
                   <Button type="submit">Validé</Button>
               </Form.Field>
           </Form>
       </Container>
       )
    }
}