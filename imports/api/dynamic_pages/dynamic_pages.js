import { Mongo } from 'meteor/mongo';

export const DynamicPages = new Mongo.Collection('dynamic_pages');

const DynamicPagesSchema = new SimpleSchema({
    title: {
        type: String
    },
    description: {
        type: String,
        optional: true
    },
    image_url: {
        type: String,
        optional: true
    },
    created_at: {
        type: Date,
        defaultValue: new Date()
    },
    active: {
        type: Boolean,
        defaultValue: true
    }

});

DynamicPages.attachSchema(DynamicPagesSchema);