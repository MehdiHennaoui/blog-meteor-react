import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import Navbar from '/imports/components/navigation/Navbar';


import AdminPage from '../page/general/AdminPage';

const adminError = () => {
    return (
        <h1>Page admin pas trouvé</h1>
    )
};

export default class AdminLayout extends Component {
    // componentWillMount() {
    //     if(!Roles.userIsInRole(Meteor.userId(), 'admin')) {
    //         Bert.alert({
    //             title: "Vous ete pas admin",
    //             message: "mais bien essayer quand meme"
    //         })
    //         this.props.history.push('/')
    //     }
    // }

    render() {
        return(
        <Grid stackable>
            <Grid.Column width={16}>
                <Navbar admin={true}/>
            </Grid.Column>
            <Grid.Column width={16}>
            <Switch>
                <Route path="/admin/pages" component={AdminPage} />
                <Route path="*" component={adminError} />
            </Switch>
            </Grid.Column>
        </Grid>
        )
    }
}