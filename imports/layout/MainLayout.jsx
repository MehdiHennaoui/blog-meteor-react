import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import { withTracker } from 'meteor/react-meteor-data';
import Landing from '../page/general/Landing'
import NotFound from '../page/general/NotFound'
import Navbar from '/imports/components/navigation/Navbar';
import ConnexionForm from '/imports/api/accounts/ConnexionForm';
import Article from '../page/Article';


class MainLayout extends Component {
    render() {
        const { user_id } = this.props;
        return(
        <Grid stackable>
            <Grid.Column width={16}>
                <Navbar/>
            <Switch>
                <Route exact path="/" component={Landing} />
                { !user_id &&
                    <Route path="/connexion" component={ConnexionForm} />
                }
                <Route path="/article/:id" component={Article} />
                <Route path="*" component={NotFound} />

            </Switch>
            </Grid.Column>
        </Grid>
            )
    }
}

export default MainLayoutContainer = withTracker(() => {
   const user_id = Meteor.userId();
    return {
       user_id,
   }
})(MainLayout)

